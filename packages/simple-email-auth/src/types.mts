export type SignupReq = { email: string; password: string; title: string }
export type ConfirmEmailPayload = { req: SignupReq }
