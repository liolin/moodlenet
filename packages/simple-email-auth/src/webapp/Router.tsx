import { Route } from 'react-router-dom'
import ConfirmEmail from './ConfirmEmail.js'

export default { routes: <Route path="confirm-email" element={<ConfirmEmail />} /> }
