import { Settings as SettingsIcon } from '@material-ui/icons'

export const text = 'Settings'
export const path = '/settings'
export const className = 'settings'
export const position = 1
export const Icon = <SettingsIcon key="settings-icon" />
