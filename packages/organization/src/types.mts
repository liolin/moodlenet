export type OrganizationData = {
  instanceName: string
  landingTitle: string
  landingSubtitle: string
  // logo: string
  // smallLogo: string
}

export type KeyValueStoreData = { organizationData: OrganizationData }
