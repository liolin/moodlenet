export type { DecryptArgs, EncryptArgs } from './lib/std-encription.mjs'
export type KVStoreTypes = {
  keypairs: {
    publicKey: string
    privateKey: string
  }
}
